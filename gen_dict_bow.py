# Copyright (c) 2022-2022 Sven Albrecht
#
# This file is part of ESSE2022
# (see https://gitlab.hrz.tu-chemnitz.de/alsv--tu-chemnitz.de/esse2022).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import gensim
import gensim.corpora as corpora
from gensim.utils import simple_preprocess
from gensim.parsing.preprocessing import STOPWORDS
from gensim import corpora, models
from gensim.models import CoherenceModel
from gensim.test.utils import datapath
from gensim.test.utils import get_tmpfile
from nltk.stem import WordNetLemmatizer, SnowballStemmer, LancasterStemmer
from nltk.stem.porter import *
import pandas as pd
import pickle
import configparser
import nltk
# initialize random number generator of numpy. If you set it to zero, random numbers will be predicable.
import numpy as np
np.random.seed(0)

# corpus_dir = './corpus'
# read configuration from ini file
config = configparser.ConfigParser()
try:
    config.read("settings.ini")
    print("read settings.ini successfully")
except:
    print("file settings.ini missing")
try:
    corpus_dir = config["corpus"]["directory"]
    no_below = config["vocabulary"]["no_below"]
    no_above = config["vocabulary"]["no_above"]
    keep_n = config["vocabulary"]["keep_n"]
except:
    print("Terrible error. Quitting.")

my_stop_words = STOPWORDS.union(set(np.loadtxt("stopwords.txt", dtype=str)))

print(my_stop_words)

corpus = nltk.corpus.reader.plaintext.PlaintextCorpusReader(corpus_dir, '.*\.txt')
data = []

for fileid in corpus.fileids():
    document = ' '.join(corpus.words(fileid))
    data.append(document)
 
NO_DOCUMENTS = len(data)

stemmer = LancasterStemmer()

def lemmatize_stemming(text):
    return stemmer.stem(WordNetLemmatizer().lemmatize(text, pos='v'))

def preprocess(text):
    result = []
    for token in gensim.utils.simple_preprocess(text):
        if token not in my_stop_words and len(token) > 3:
            result.append(lemmatize_stemming(token))
    return result

processed_docs = []

for sent in data:
    processed_docs.append((preprocess(sent)))

dictionary = gensim.corpora.Dictionary(processed_docs)



count = 0
for k, v in dictionary.iteritems():
    print(k, v)
    count += 1
    if count > 10:
        break
dictionary.filter_extremes(no_below=no_below, no_above=no_above, keep_n=keep_n)
bow_corpus = [dictionary.doc2bow(doc) for doc in processed_docs]

output_dict = open('dictionary.pkl', 'wb')
output_bow = open('bow.pkl', 'wb')
output_texts = open('texts.pkl', 'wb')
pickle.dump(dictionary, output_dict)
pickle.dump(bow_corpus, output_bow)
pickle.dump(processed_docs, output_texts)
output_dict.close()
output_bow.close()
output_texts.close()